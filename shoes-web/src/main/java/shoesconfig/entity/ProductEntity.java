package shoesconfig.entity;

import com.sun.org.apache.xml.internal.resolver.CatalogEntry;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="product")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private float price;
    @Column(name = "rating")
    private float rating;
    @Column(name = "size")
    private int size;
    @Column(name = "detail")
    private String detail;
    @Column(name = "highlight")
    private String highlight;
    @Column(name = "description")
    private String description;
    @Column(name = "terms")
    private String terms;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private CategoryEntity category;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    private List<PhotoEntity> photoList;

    @OneToMany(mappedBy = "product")
    private List<OrderDetailEntity> orderDetailList;

    public ProductEntity() {
    }

    public ProductEntity(int id, String name, float price, float rating, int size, String detail, String highlight, String description, String terms) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.rating = rating;
        this.size = size;
        this.detail = detail;
        this.highlight = highlight;
        this.description = description;
        this.terms = terms;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public List<PhotoEntity> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<PhotoEntity> photoList) {
        this.photoList = photoList;
    }

    public List<OrderDetailEntity> getOrderDetailEntities() {
        return orderDetailList;
    }

    public void setOrderDetailEntities(List<OrderDetailEntity> orderDetailEntities) {
        this.orderDetailList = orderDetailEntities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }
}
