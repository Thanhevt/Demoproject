package shoesconfig.repository;

import org.springframework.data.repository.CrudRepository;
import shoesconfig.entity.ProductEntity;


public interface ProductRepository extends
        CrudRepository<ProductEntity, Integer> {
}
