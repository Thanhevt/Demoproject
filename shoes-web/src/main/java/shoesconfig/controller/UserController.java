package shoesconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import shoesconfig.entity.RoleEntity;
import shoesconfig.entity.UserEntity;
import shoesconfig.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegisterPage(Model model) {
        model.addAttribute("user",new UserEntity());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String submitRegisterPage(Model model, UserEntity user) {
        if (userRepository.findByEmail(user.getEmail())!=null){
            model.addAttribute("error","Email existing");
            model.addAttribute("user",user);
            return "register";
        }
        RoleEntity role = new RoleEntity();
        role.setId(1);
        user.setRole(role);
        userRepository.save(user);
        return "redirect:/";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin(Model model){
        model.addAttribute("user", new UserEntity());
        return "signup";

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String submitLogin(Model model, UserEntity user, HttpServletRequest request) {
        UserEntity findUser = userRepository.findByEmailAndPassword(user.getEmail(),user.getPassword());
        if (findUser==null){
            model.addAttribute("error","User name or Password is incorrect");
            model.addAttribute("user",user);
            return "signup";
        }

        //login successfully
        HttpSession session = request.getSession();
        session.setAttribute("email",user.getEmail());
        System.out.println(user.getEmail());
        return "redirect:/";
    }

    @RequestMapping(value = "/logout")
    public String submitLogout(HttpServletRequest request){
        request.getSession().invalidate();
        return "redirect:/";
    }

}
