package shoesconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import shoesconfig.entity.ProductEntity;
import shoesconfig.repository.ProductRepository;
import java.util.List;


@Controller
@RequestMapping(value = "/")
public class HomeController {

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping
    public String showIndex(Model model) {
        List<ProductEntity> productEntityList =
                (List<ProductEntity>) productRepository.findAll();
        model.addAttribute("productList", productEntityList);
        return "index";
    }

}
